<?php

require "../bootstrap.php";

include "../controllers/ApiController.php";

use controllers\ApiController;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode('/', $uri);

$cityName = null;
$cityId = null;

// all of endpoints started with cache
($uri[1] === 'cache') ?: die("Uncorrect Url, please use /cache ex. http://127.0.0.1:8000/cache/get-city/Bialystok");
($uri[1] === 'cache' && $uri[2] === 'get-city') ? $cityName = (string)$uri[3] : '';
($uri[1] === 'cache' && $uri[2] === 'delete') ? $cityName = (string)$uri[3] : '';
($uri[1] === 'cache' && $uri[2] > 0) ? $cityId = (string)$uri[2] : '';

//$cityName = $utf8_string = Encoding::toUTF8($cityName);

$requestMethod = $_SERVER["REQUEST_METHOD"];

if ($uri[1] === 'cache') {
    $controller = new ApiController($dbConnection, $requestMethod, $cityName);
    $controller->processRequest();
}

function charsetUtfFix($string) {

    $utf = array(
        "%u0104" => "Ą",
        "%u0106" => "Ć",
        "%u0118" => "Ę",
        "%u0141" => "Ł",
        "%u0143" => "Ń",
        "%u00D3" => "Ó",
        "%u015A" => "Ś",
        "%u0179" => "Ź",
        "%u017B" => "Ż",
        "%u0105" => "ą",
        "%u0107" => "ć",
        "%u0119" => "ę",
        "%u0142" => "ł",
        "%u0144" => "ń",
        "%u00F3" => "ó",
        "%u015B" => "ś",
        "%u017A" => "ź",
        "%u017C" => "ż"
    );

    return str_replace(array_keys($utf), array_values($utf), $string);

}

