import React from 'react';
import '../styles/Header.css';

import nav_icon from "../images/nav_icon.png"

const Header = () => {
    return (
        <>
            <div className="logoTyp">
                <div className="icon">
                    <img src={nav_icon} alt="nav_icon"/>
                </div>
                <div className="navText">
                    <span className="city">city</span>
                    <span className="geo">geo</span>
                </div>
            </div>
        </>
    )
};

export default Header