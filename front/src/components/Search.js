import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import "../styles/Search.css"

const Search = (props) => {
    const result = props.result;

    const content = result.map(city => (
        <div key='result'>
            <strong>Wynik: </strong>
            lat: <strong>{parseFloat(city.lat).toFixed(4)}</strong>,
            lon: <strong>{parseFloat(city.lon).toFixed(4)}</strong>,
            <strong>{city.country}</strong>
        </div>
    ));

    const empty = <span style={{color: 'gray'}} >Brak wyników</span>;

    return (
        <>
            <div className="searchForm">
                <div className="titleContainer">
                    <div className="title">City Search</div>
                    <div className="title2">Find Your Best Place</div>
                </div>

                <div className="inputContainer">
                    <label htmlFor="city name">Wpisz nazwę miasta</label>
                    <input type="text" className="cityInput" id="city" placeholder="np. Warszawa" onChange={props.input}
                           required>
                    </input>
                </div>

                <div className="resultContainer">
                    {(result.length ? content : empty)}
                </div>

                <div className="buttonContainer">
                    <button type="button" className="button buttonSearch" onClick={props.click}>Search</button>
                </div>
            </div>
        </>
    )
};

export default Search

