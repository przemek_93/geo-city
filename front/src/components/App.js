import React, {Component} from 'react';

import '../styles/App.css';
import Header from "./Header";
import Footer from "./Footer";
import Content from "./Content";
import Search from "./Search";

class App extends Component {

    allCitiesUrl = `http://127.0.0.1:8000/cache/get-city/`;
    delAllCitiesUrl = `http://127.0.0.1:8000/cache/delete/`;

    state = {
        cityUrl: '',
        delCityUrl: '',
        city: [],
        allCities: [],
    };

    handleCityFetch = () => {
        fetch(this.state.cityUrl)
            .then(response => {
                if (response.ok) {
                    return response;
                }
                throw Error(response.status)
            })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    city: data,
                    cityUrl: ''
                });
            })
            .catch(error => console.log(error + " Error!!! "));
    };

    handleAllCitiesFetch = () => {
        fetch(this.allCitiesUrl)
            .then(response => {
                if (response.ok) {
                    return response;
                }
                throw Error(response.status)
            })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    allCities: data,
                    cityUrl: ''
                })
            })
            .catch(error => console.log(error + " Error!!! "));
    };

    handleDeleteCityFetch = (url) => {
        fetch(url, {method: 'DELETE'})
            .then(response => {
                if (response.ok) {
                    return response;
                }
                throw Error(response.status)
            })
            .then(response => response.json())
            .then(this.handleAllCitiesFetch())
            .catch(error => console.log(error + " Error!!! "));
    };

    validateInput = () => {
        const cityUrl = this.state.cityUrl;

        if (cityUrl.length === 0 || cityUrl === 'http://127.0.0.1:8000/cache/get-city/') {
            alert("Proszę podać miasto");
        }

        if (/\d/.test(cityUrl.substring(37))) {
            alert("Niedozwolone znaki w nazwie");
        }
    };

    handleSearchClick = () => {
        this.validateInput();
        this.handleCityFetch();
        this.handleAllCitiesFetch();
    };

    handleCityInput = (e) => {
        this.setState({
            cityUrl: `http://127.0.0.1:8000/cache/get-city/${e.target.value}`,
        });
    };


    handleDelCityButton = (cityName) => {
        this.handleDeleteCityFetch(`http://127.0.0.1:8000/cache/delete/${cityName}`);
    };

    handleDelAllCityButton = () => {
        this.handleDeleteCityFetch(this.delAllCitiesUrl);
    };

    componentDidMount() {
        this.handleAllCitiesFetch();
    }

    render() {

        const {city, allCities} = this.state;

        return (
            <>
                <div className="app">
                    <header>
                        {<Header/>}
                    </header>

                    <section className="main">
                        <div className="searchContainer">
                            {<Search click={this.handleSearchClick} input={this.handleCityInput}
                                     result={city}/>}
                        </div>
                    </section>

                    <section className="table">
                        <div className="contentTable">
                            {<Content data={allCities} deleteCity={this.handleDelCityButton}
                                      deleteAllCity={this.handleDelAllCityButton}/>}
                        </div>
                    </section>

                    <footer>
                        {<Footer/>}
                    </footer>
                </div>
            </>
        );
    }
}

export default App;
