import React from 'react';
import shortid from 'shortid';

import '../styles/Content.css';

const Content = (props) => {
    const {data, deleteCity, deleteAllCity} = props;

    const list = data.map(data => (
        <div className="row listing" key={shortid.generate()}>
            <div className="col item" key={shortid.generate()}>{data.name}</div>
            <div className="col item" key={shortid.generate()}>{data.lon}</div>
            <div className="col item" key={shortid.generate()}>{data.lat}</div>
            <div className="col item" key={shortid.generate()}>{data.date}</div>
            <div className="col item" key={shortid.generate()}>{data.count}</div>
            <div className="col item" key={shortid.generate()}>
                <button type="button" className="button buttonClear" onClick={() => deleteCity(data.name)}>Clear Entry
                </button>
            </div>
        </div>
    ));

    const clearAll = <div className="row justify-content-end">
        <div className="col-2 float-right">
            <button type="button" className="button buttonClear" onClick={deleteAllCity}>Clear All</button>
        </div>
    </div>;

    const empty = <span style={{color: 'gray', fontSize: 'small'}}>Brak danych</span>;

    return (
        <>
            <h4 className="cache">Cache</h4>
            <div className="row tableHeader">
                <div className="col itemHeader" key="Name">Name</div>
                <div className="col itemHeader" key="Longitude">Longitude</div>
                <div className="col itemHeader" key="Latitude">Latitude</div>
                <div className="col itemHeader" key="Date">Date</div>
                <div className="col itemHeader" key="Count">Log Count</div>
                <div className="col itemHeader" key="Action">Action</div>
            </div>

            {list}

            {(data.length) ? clearAll : empty}
        </>
    )
};

export default Content