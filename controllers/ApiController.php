<?php

namespace controllers;

include "../models/Model.php";

use models\Model;

class ApiController
{
    private $db;
    private $requestMethod;
    private $cityName;
    private $gateway;
    private $cacheTable = 'cache';
    private $logTable = 'log';

    public function __construct($db, $requestMethod, $cityName)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->cityName = $cityName;
        $this->gateway = new Model($db);
    }

    /*
     * Check request method and route.
     */
    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->cityName) {
                    $name = $this->changePolishSign($this->cityName);
                    $response = $this->getCacheByName($name);
                } else {
                    $data = $this->getAllCache();
                    $response = $this->withCount($data);
                };
                break;
            case 'DELETE':
                $response = $this->deleteCache($this->cityName);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }

    }

    /*
     * Returns all caches.
     */
    private function getAllCache()
    {
        $result = $this->gateway->findAll($this->cacheTable);
        $response['status_code_header'] = 'HTTP/1.1 200 Get';
        $response['body'] = json_encode($result);

        return $response;
    }

    /*
     * Returns cache by city name.
     * If cache table dont have specific records, route to external api .
     */
    private function getCacheByName($name)
    {
        $name = strtolower($name);
        $result = $this->gateway->find($name, $this->cacheTable);

        if (!$result) {
            $result = $this->getLocationFromExternalApi($name);
            $this->insertLog($result, $name);
        } else {
            $this->insertLog($result, $name);
        }

        $response['status_code_header'] = 'HTTP/1.1 200 Get';
        $response['body'] = json_encode($result);

        return $response;
    }

    /*
     * Get count of query about this same city
     */
    private function withCount($data)
    {
        $items = json_decode($data['body'], true);
        $result = [];

        foreach ($items as $item) {
            $count = $this->gateway->getCount($item['id']);
            $item['count'] = count($count);
            $result[] = $item;
        }

        $data['body'] = json_encode($result);

        return $data;
    }

    /*
     * Insert log after successfully returns cache by name.
     */
    private function insertLog($result, $name)
    {
        $input = [
            'name' => 'zapytanie o ' . $name . ' ' . date('Y-m-d H:i:s'),
            'date' => date('Y-m-d'),
            'query_ip' => $_SERVER['REMOTE_ADDR'],
            'cache_id' => $result[0]['id'],
        ];

        $this->gateway->insert($input, $this->logTable);
    }

    /*
     * Delete specific records or all caches.
     */
    private function deleteCache($name)
    {
        $result = $this->gateway->find($name, $this->cacheTable);

        if (!$result) {
            $this->gateway->deleteAll($this->cacheTable);
        } else {
            $this->gateway->delete($name, $this->cacheTable);
        }

        $response['status_code_header'] = 'HTTP/1.1 200 Deleted';
        $response['body'] = null;

        return $response;
    }

    /*
     * Get location from external api.
     */
    private function getLocationFromExternalApi($city)
    {

        $url = 'https://nominatim.openstreetmap.org/search?q=' . $city . '&format=json&limit=1&email=testxampp12@gmail.com';

        $json = $this->curlGetRequest($url);
        $result = json_decode($json, true);
        if (!$result) {
            return $this->notFoundResponse();
        } else {
            $name = $result[0]['display_name'];
            $name = $this->changePolishSign($name);
            $partName = explode(',', $name);
            $result = [0 => [
                'name' => $partName[0],
                'lon' => $result[0]['lon'],
                'lat' => $result[0]['lat'],
                'date' => date('Y-m-d'),
                'country' => $partName[2],
            ]
            ];

            $input = array_map('strtolower', $result[0]);
            $this->gateway->insert($input, $this->cacheTable);
        }

        return $result;
    }

    /*
     * Handle api query.
     */
    private function curlGetRequest($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    /*
     * Changing polish sign.
     */
    private function changePolishSign($str)
    {
        $search = ['Ą', 'ą', 'Ć', 'ć', 'Ę', 'ę', 'Ł', 'ł', 'Ń', 'ń', 'Ó', 'ó', 'Ś', 'ś', 'Ź', 'ź', 'Ż', 'ż'];
        $replace = ['A', 'a', 'C', 'c', 'E', 'e', 'L', 'l', 'N', 'n', 'O', 'o', 'S', 's', 'Z', 'z', 'Z', 'z'];

        return str_replace($search, $replace, $str);
    }

    /*
     * Generate message after fail response.
     */
    private function notFoundResponse()
    {
        $response = [];

        return $response;
    }
}