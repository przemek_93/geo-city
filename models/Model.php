<?php

namespace models;

class Model
{

    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    /*
     * Get all cache.
     */
    public function findAll($table)
    {
        $statement = "
            SELECT 
             id, name, lon, lat, date
            FROM
                $table;
        ";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Get cache by name.
     */
    public function find($name, $table)
    {
        $statement = "
            SELECT 
                id, name, lon, lat, date, country
            FROM
                $table
            WHERE name = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([$name]);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Insert records into table.
     */
    public function insert(Array $input, $table)
    {
        if ($table === 'cache') {
            $statement = "
            INSERT INTO cache 
                (name, lon, lat, date, country)
            VALUES
                (:name, :lon, :lat, :date, :country);
        ";
        } elseif ($table === 'log') {
            $statement = "
            INSERT INTO log 
                (name, date, query_ip, cache_id)
            VALUES
                (:name, :date, :query_ip, :cache_id);
        ";
        }
        try {
            $statement = $this->db->prepare($statement);
            if ($table === 'cache') {
                $statement->execute([
                    'name' => $input['name'],
                    'lon' => $input['lon'],
                    'lat' => $input['lat'],
                    'date' => $input['date'],
                    'country' => $input['country'],
                ]);
            } else {
                $statement->execute([
                    'name' => $input['name'],
                    'date' => $input['date'],
                    'query_ip' => $input['query_ip'],
                    'cache_id' => $input['cache_id'],
                ]);
            }

            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Get count of query about this same city.
     */
    public function getCount($id)
    {
        $statement = "
            SELECT
                name, date
            FROM
                log
            WHERE cache_id = ?;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute([$id]);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Delete records by name.
     */
    public function delete($name, $table)
    {
        $statement = "
            DELETE FROM $table
            WHERE name = :name;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(['name' => $name]);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Delete all records.
     */
    public function deleteAll($table)
    {
        $statement = "
            DELETE FROM $table;
        ";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(['name' => $name]);
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

}