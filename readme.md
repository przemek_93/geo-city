## Geo City

#### Instalacja

Zaimportować załączony zrzut bazy, dodaje zrzut z programu DataGrip jak i PhpMyAdmin jakby coś było nie tak.
W .env ustawić parametry połączenia. W przypadku klonowania z repozytorium dodatkowo odpalić:

```
w katalogu głównym geoCity/: composer install  
w geoCity/front: npm install
```

#### Uruchomienie

Po podpięciu bazy danych, aby aplikacja działała należy uruchomić najpierw komendy:
```
w katalogu głównym geoCity/: php -S 127.0.0.1:8000 -t public 
w geoCity/front: npm start
```

